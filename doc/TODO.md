# Validos
* [ok] Validação de tipo de objeto de saída em função
* [ok] Implementar suporte a identificador relativo de elemento em dataobject
* Implementar tipos com processamento customizado, para coleções e arrays
* Implementar suporte à renderização customizada de elementos por dataobject
* Implementar suporte à tracking de página
* Implementar suporte à localização
* [ok] Implementar indicador de itens filhos em datatype (substituindo items().size())

## Rebaixados
* Implementar atribuição de exhandler padrão para alvos da página
