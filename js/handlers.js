async function qs_request(url, data) {
   const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
   });
   
   return response.json();
}

function qs_listener_register(control, event) {
   let inputs = document.querySelectorAll('[data-ic-type]');
   let items = {};
   
   for (i in inputs)
     if ((inputs[i].id != null) && (inputs[i].dataset.icType != null))
        items[inputs[i].dataset.icType] = inputs[i].value;
   
   items['context.target'] = control.target;
   if (control.sourcep)
      items[control.sourcep] = control.sourcev;
   
   qs_request('/qssrm/', {
      items: items
   }).
   then(data=>{
      if (data != null) {
         if (data.items != null)
            for (i in data.items) {
               let item = data.items[i];
               let element = document.getElementById(item.name);
               element[element.dataset.icEntryPoint] = item.value;
            }
         
         if (data.elementsupdt != null)
            for (i in data.elementsupdt) {
               let elementupdt = data.elementsupdt[i];
               let element = document.getElementById(elementupdt.name);
               if (elementupdt.content)
                  element.outerHTML = elementupdt.content;
               if (elementupdt.attributes)
                  for (i in elementupdt.attributes) {
                     let attribute = elementupdt.attributes[i];
                     element[attribute.name] = attribute.value;
                  }
            }
         
         if (data.target != null) {
            document.getElementById('context.target').value = data.target;
            if (data.form == null)
               throw 'undefined data.form';
            let form = document.getElementById(data.form);
            form.action = data.action;
            form.method = 'POST';
            form.submit();
         }
      }
   });
   
   event.preventDefault();
}

function qs_register_all(facility, page) {
   
	$( document ).ready(function() {
		let target = facility.concat('.').
			concat(page).
			concat('_controls_get');
			
		qs_request('/qssrm/', {
			target: target
		}).
		then(controls=>{
			for (i in controls.items) {
			    let control = controls.items[i];
			    let element = document.getElementById(control.name);
			    element.addEventListener(
			       control.event,
			       function(event) {
			          qs_listener_register(control, event);
			       });
			}
		});
	});
}

