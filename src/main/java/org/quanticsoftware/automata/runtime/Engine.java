package org.quanticsoftware.automata.runtime;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.facilities.SharedContext;

public class Engine {
	private Context context;
	private AutomataProgram program;
	
	public Engine(Context context, AutomataProgram program) {
		this.context = context;
		this.program = program;
	}
	
	public final DataObject execute(DataContext datactx) throws Exception {
		var values = new HashMap<String, DataObject>();
		if (datactx != null)
			for (var key : datactx.instances())
				values.put(key, datactx.get(key));
		return execute(values);
	}
	
	private final DataObject execute(Map<String, DataObject> values) throws Exception {
		var ldatactx = new DataContext();
		DataObject value = null;
		
		for (var item : program.items()) {
			var facility = context.getFacility(item.facility);
			var config = facility.get(item.function).getConfig();
			
			var shctx = new SharedContext();
			shctx.datactx = ldatactx;
			shctx.context = context;
			shctx.fncname = item.function;
			shctx.outputtype = config.output;
			
			for (var key : item.input.keySet()) {
				var iitem = item.input.get(key);
				if (iitem.data != null) {
					shctx.input.put(key, values.get(iitem.data));
					continue;
				}
				
				var subprogram = Engine.instance(context, iitem.program);
				
				shctx.input.put(key, subprogram.execute(values));
			}
			
			value = facility.run(item.function, shctx);
			var oname = Concatenate.execute("output.", item.id);
			values.put(oname, value);
		}
		
		return value;
	}
	
	public static final Engine instance(
			Context context,
			AutomataProgram program) {
		return new Engine(context, program);
	}
}
