package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.core.Context;

public class AutomataProgramContext {
	public Context context;
	public int level;
	public String id;
	public ProgramItem[] items;
}
