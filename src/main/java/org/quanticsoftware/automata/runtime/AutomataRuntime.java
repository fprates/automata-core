package org.quanticsoftware.automata.runtime;

import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.Fail;

public class AutomataRuntime {
	
	public static final DataObject execute(
			Context context,
			AutomataProgram program,
			DataContext input) throws Exception {
		if (program == null)
			Fail.raise("can't execute undefined program.");
		
		return Engine.instance(context, program).execute(input);
	}
}
