package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.facilities.FunctionConfig;

public class Template {
	public String function;
	public Map<String, OriginalFunction> functions;
	public FunctionConfig config;
	public boolean parameter;
	
	public Template() {
		functions = new HashMap<>();
	}
}

