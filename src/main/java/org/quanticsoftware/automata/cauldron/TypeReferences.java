package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.Map;

public class TypeReferences {
	private Map<String, Map<String, Supplier>> refs;
	
	public TypeReferences() {
		refs = new HashMap<>();
	}
	
	public final void clear() {
		refs.clear();
	}
	
	public final Map<String, Supplier> get(String type) {
		return refs.get(type);
	}
	
	public final Map<String, Supplier> instance(String type) {
		var references = new HashMap<String, Supplier>();
		refs.put(type, references);
		return references;
	}
	
	public final void put(String type, Map<String, Supplier> ref) {
		refs.put(type, ref);
	}
}

