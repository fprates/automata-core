package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.quanticsoftware.automata.core.Fail;

public class Span<K,T,V> {
	private Stack<SpanItem<K, V>> stack;
	private K[] parameters;
	private List<Map<K, V>> results;
	private ReferencesGet<T,V> referencesget;
	private Map<K, T> output;
	
	public Span() {
		stack = new Stack<>();
	}
	
	@SuppressWarnings("unchecked")
	public final List<Map<K, V>> execute(
			Map<K, T> output,
			ReferencesGet<T,V> referencesget) {
		var i = 0;
		this.referencesget = referencesget;
		this.output = output;
		
		parameters = (K[])new Object[output.size()];
		for (var key : output.keySet())
			parameters[i++] = key;
		
		stack.clear();
		results = new LinkedList<>();
		span(0);
		
		return results;
	}

	private final void span(int i) {
		var otype = output.get(parameters[i]);
		var references = referencesget.execute(otype);
		if (references == null)
			Fail.raise("no references for '%s'.", otype.toString());
		
		for (var key : references.keySet()) {
			var item = new SpanItem<K, V>();
			item.name = parameters[i];
			item.value = references.get(key);
			stack.push(item);
			
			var n = i + 1;
			if (n < parameters.length) {
				span(n);
				stack.pop();
				continue;
			}
			
			var result = new HashMap<K, V>();
			for (var e : stack)
				result.put(e.name, e.value);
			results.add(result);
			stack.pop();
		}
	}
}

interface ReferencesGet<T,V> {
	
	public abstract Map<String, V> execute(T name);
	
}
