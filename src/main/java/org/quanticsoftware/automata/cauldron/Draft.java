package org.quanticsoftware.automata.cauldron;

import org.quanticsoftware.automata.core.Concatenate;

public class Draft {
	public int level, id, parent;
	public DraftOption option;
	public Supplier supplier;

	@Override
	public final String toString() {
		return Concatenate.execute(
				(option == null)? "" : option.toString(),
				": ",
				(supplier == null)? "" : supplier.toString());
	}
}

