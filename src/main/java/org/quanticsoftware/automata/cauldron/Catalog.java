package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Catalog {
	public int dc;
	public Map<Integer, Draft> draftids;
	public Map<String, Template> templates;
	public int maxlevel, iconn, last;
	public Set<Integer> resume;
	public Map<Integer, List<Supplier>> groups;
	public TypeReferences otyperefs;
	public Map<String, Map<Integer, Map<Integer, Map<String, Supplier>>>> options;
	public boolean suspended;
	public Draft first;
	
	public Catalog() {
		draftids = new HashMap<Integer, Draft>();
		otyperefs = new TypeReferences();
		templates = new HashMap<String, Template>();
		options = new HashMap<>();
		resume = new HashSet<>();
	}
	
	public Draft draft() {
		var draft = new Draft();
		draft.id = ++dc;
		return draft;
	}
}
