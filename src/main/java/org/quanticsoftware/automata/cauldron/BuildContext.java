package org.quanticsoftware.automata.cauldron;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.quanticsoftware.automata.runtime.AutomataProgramContext;
import org.quanticsoftware.automata.runtime.ProgramItem;

public class BuildContext {
	public AutomataProgramContext programctx;
	public Map<Integer, ProgramItem> pitems;
	public List<ProgramItem> items;
	
	public BuildContext() {
		pitems = new HashMap<>();
		items = new LinkedList<>();
	}
}
