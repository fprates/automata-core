package org.quanticsoftware.automata.lib;

public interface UnitConversionRule {
	
	public abstract double convert(double input);
	
}
