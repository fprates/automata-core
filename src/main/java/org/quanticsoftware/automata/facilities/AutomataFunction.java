package org.quanticsoftware.automata.facilities;

import java.util.HashSet;

import org.quanticsoftware.automata.cauldron.Cauldron;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.core.Fail;

public class AutomataFunction implements Function {
	private FunctionConfig functioncfg;
	private FunctionRule rule;
	private String name;
	
	public AutomataFunction(String name) {
		this.name = name;
		functioncfg = new FunctionConfig();
	}
	
	@Override
	public final void allinput(DataType output, FunctionRule rule) {
		functioncfg.allinput = true;
		rule(output, rule);
	}
	
	@Override
	public final void bypass(DataType type, FunctionRule rule) {
		mtinput(type);
		rule(type, rule);
	}
	
	@Override
	public final void collect(DataType collection, String field) {
		var output = collection.get(field);
		var cname = collection.name();
		
		if (collection.isReference(field))
			allinput(output, s->refsplit(s, cname, field));
		else
			allinput(output, s->split(s, cname, field));
		
	}
	
	@Override
	public final FunctionConfig getConfig() {
		return functioncfg;
	}
	
	@Override
	public final void input(DataType type, String... names) {
		if (type == null)
			Fail.raise("undefined input type for function '%s'.", name);
		
		for (var name : names) {
			functioncfg.inputs.put(name, type);
			functioncfg.iconfig.put(name, new HashSet<>());
		}
	}

	@Override
	public final void mtinput(DataType... types) {
		for (var type : types) {
			var name = type.name();
			functioncfg.inputs.put(name, type);
			functioncfg.iconfig.put(name, new HashSet<>());
		}
	}
	
	@Override
	public final void property(Cauldron.property property) {
		functioncfg.properties.add(property);
	}
	
	@Override
	public final void property(String name, Cauldron.property value) {
		functioncfg.iconfig.get(name).add(value);
	}
	
	private final DataObject refsplit(
			Session session,
			String cname,
			String tname) {
		var collection = session.get(cname);
		return (DataObject)collection.get(tname);
	}
	
	@Override
	public final void reject(String function) {
		functioncfg.rejected.add(function);
	}
	
	@Override
	public final FunctionRule rule() {
		return rule;
	}
	
	@Override
	public final void rule(DataType output, FunctionRule rule) {
		functioncfg.output = output;
		this.rule = rule;
	}
	
	@Override
	public final DataObject run(SharedContext shctx) throws Exception {
		var session = new Session(shctx);
		var output = rule.execute(session);
		
		if (output == null)
			Fail.raise("null output parameter on '%s'.", shctx.fncname);
			
		if (!output.type().equals(shctx.outputtype))
			Fail.raise("incompatible output parameter on '%s'. '%s' type is expected.",
					shctx.fncname,
					shctx.outputtype.name());
			
		return output;
	}

	private final DataObject split(
			Session session,
			String cname,
			String tname) {
		var collection = session.get(cname);
		
		var citem = collection.data(tname);
		var output = session.output(citem);
		var otype = output.type();
		
		for (var okey : otype.items())
			if (otype.isReference(okey))
				output.set(okey, session.get(otype.get(okey).name()));
		
		return output;
	}
}
