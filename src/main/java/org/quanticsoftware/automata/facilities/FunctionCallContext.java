package org.quanticsoftware.automata.facilities;

import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;

public class FunctionCallContext {
	private SharedContext shctx;
	private String facility, function;
	
	public FunctionCallContext(
			SharedContext shctx,
			String facility,
			String function) {
		
		var config = shctx.context.
				getFacility(facility).
				get(function).
				getConfig();
		
		this.shctx = new SharedContext();
		this.shctx.context = shctx.context;
		this.shctx.outputtype = config.output;
		this.shctx.datactx = new DataContext();
		
		for (var key : config.inputs.keySet()) {
			var name = Concatenate.execute(facility, ".", function, ":", key);
					
			this.shctx.input.put(
					key,
					shctx.datactx.instance(config.inputs.get(key), name));
		}
		
		this.facility = facility;
		this.function = function;
	}
	
	public final DataObject call() throws Exception {
		return shctx.context.getFacility(facility).run(function, shctx);
	}
	
	public final DataObject input(String name) {
		return shctx.input.get(name);
	}
}
