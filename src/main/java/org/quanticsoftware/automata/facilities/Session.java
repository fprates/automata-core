package org.quanticsoftware.automata.facilities;

import java.util.Map;

import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.core.ErrorMessageException;

public class Session {
	private DataObject output;
	private SharedContext shctx;
	
	public Session(SharedContext shctx) {
		this.shctx = shctx;
	}
	
	public final void fail(String text, Object... args) {
		throw new ErrorMessageException(text, args);
	}
	
	public final DataObject get(String name) {
		return shctx.input.get(name);
	}
	
	public final double getd(String name) {
		return get(name).getd("value");
	}
	
	public final int geti(String name) {
		return get(name).geti("value");
	}
	
	public final long getl(String name) {
		return get(name).getl("value");
	}
	
	public final String getst(String name) {
		return get(name).getst("value");
	}
	
	public final Object getv(String name) {
		return get(name).get("value");
	}
	
	public final void noupdate() {
		shctx.noupdate = true;
	}
	
	public final DataObject output() {
		return output(null);
	}
	
	public final DataObject output(Map<String, Object> data) {
		if (output != null)
			return output;
		
		String oname = null;
		if (data == null)
			oname = shctx.datactx.instance(shctx.outputtype);
		else
			oname = shctx.datactx.instance(shctx.outputtype, data);
		
		return output = shctx.datactx.get(oname);
	}
	
	public final String sessionid() {
		return shctx.sessionid;
	}
	
	public final DataType type(String name) {
		return shctx.context.typectx().get(name);
	}
}
