package org.quanticsoftware.automata.facilities;

import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.DataType;

public class SharedContext {
	public Context context;
	public DataType outputtype;
	public DataContext datactx;
	public Map<String, DataObject> input;
	public boolean noupdate;
	public String fncname, sessionid;
	
	public SharedContext() {
		input = new HashMap<>();
	}
}
