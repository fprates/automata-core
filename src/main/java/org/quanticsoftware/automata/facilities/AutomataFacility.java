package org.quanticsoftware.automata.facilities;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.core.DataObject;

public class AutomataFacility implements Facility {
	private Map<String, Function> functions;
	private String name;
	
	public AutomataFacility(String name) {
		functions = new HashMap<>();
		this.name = name;
	}
	
	@Override
	public final Function function(String name) {
		return functions.computeIfAbsent(name, AutomataFunction::new);
	}

	@Override
	public final Set<String> functions() {
		return functions.keySet();
	}
	
	@Override
	public final Function get(String name) {
		return functions.get(name);
	}
	
	@Override
	public final String name() {
		return name;
	}
	
	@Override
	public final DataObject run(String function, SharedContext shctx)
			throws Exception {
		return functions.get(function).run(shctx);
	}
}

