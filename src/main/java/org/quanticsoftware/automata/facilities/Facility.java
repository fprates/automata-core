package org.quanticsoftware.automata.facilities;

import java.util.Set;

import org.quanticsoftware.automata.core.DataObject;

public interface Facility {
	
	public abstract Function function(String name);

	public abstract Set<String> functions();
	
	public abstract Function get(String name);
	
	public abstract String name();
	
	public abstract DataObject run(String function, SharedContext shctx)
			throws Exception;
}
