package org.quanticsoftware.automata.core;

public class Concatenate {

	public static final String execute(Object... args) {
		if (args.length == 1)
			return args[0].toString();
		
		var sb = new StringBuilder();
		for (var arg : args)
			sb.append(arg);
		
		return sb.toString();
	}
}
