package org.quanticsoftware.automata.core;

public class ErrorMessageException extends GeneralException {
	private static final long serialVersionUID = 3962345460370754365L;
	public String text;
	public Object[] args;
	
	public ErrorMessageException(Exception e) {
		super(e.getMessage());
	}
	
	public ErrorMessageException(String text, Object[] args) {
		super(text, args);
		this.text = text;
		this.args = args;
	}

}
