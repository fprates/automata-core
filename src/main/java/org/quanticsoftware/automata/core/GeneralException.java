package org.quanticsoftware.automata.core;

public class GeneralException extends RuntimeException {
	private static final long serialVersionUID = -6496785139921201162L;

    public GeneralException(String text, Object... args) {
        super(String.format(text, args));
    }

}
