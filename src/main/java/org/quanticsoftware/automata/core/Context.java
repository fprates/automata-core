package org.quanticsoftware.automata.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.core.target.Target;
import org.quanticsoftware.automata.facilities.AutomataFacility;
import org.quanticsoftware.automata.facilities.Facility;

public class Context {
	private TypeContext typectx;
	private DataType ctxtype;
	private Map<String, Facility> facilities;
	private Map<String, Target> targets;
	private Map<String, String[]> aliases;
	
	public Context() {
		typectx = new TypeContext();
		facilities = new HashMap<>();
		targets = new HashMap<>();
		aliases = new HashMap<>();
	}
	
	public final String[] alias(String alias) {
		return aliases.get(alias);
	}
	
	public final void alias(String alias, String fakey, String fnkey) {
		aliases.put(alias, new String[] {fakey, fnkey});
	}
	
	public final Set<String> facilities() {
		return facilities.keySet();
	}
	
	public final Facility facility(String name) {
		return facilities.computeIfAbsent(name, AutomataFacility::new);
	}
	
	public final void facility(String name, Facility facility) {
		facilities.put(name, facility);
	}
	
	public final DataType getContextType() {
		return ctxtype;
	}
	
	public final Facility getFacility(String name) {
		return facilities.get(name);
	}
	
	public final Target getTarget(String name) {
		return targets.get(name);
	}
	
	public final void setContextType(DataType ctxtype) {
		this.ctxtype = ctxtype;
	}
	
	public final Target target(String name) {
		return targets.computeIfAbsent(name, Target::new);
	}
	
	public final Set<String> targets() {
		return targets.keySet();
	}
	
	public final TypeContext typectx() {
		return typectx;
	}
}
