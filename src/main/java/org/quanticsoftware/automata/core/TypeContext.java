package org.quanticsoftware.automata.core;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TypeContext {
	private Map<String, DataType> types;
	private int anonc;
	private Map<String, String> boxed;
	private boolean sealed;
	
	public TypeContext() {
		types = new HashMap<>();
		boxed = new HashMap<String, String>();
		
		primitive("boolean", s->false);
		primitive("int", s->0);
		primitive("long", s->0l).set(e->e.oconversion = (t,s)->(long)(int)s);
		primitive("string", s->null);
		define(null, "any", s->null);
		primitive("char", s->'\u0000');
		primitive("double", s->0.0);
		
		var amount = define(null, "amount", s->new HashMap<String, Object>());
		amount.addd("value");
		amount.addst("unit");
	}
	
	public final DataType define(String name) {
		return define(
				null,
				name,
				(InitValue)c->c.isstructure?
						new HashMap<String, Object>() : null);
	}
	
	public final DataType define(DataType parent, String name) {
		return define(
				parent.aname(),
				name,
				(InitValue)c->c.isstructure?
						new HashMap<String, Object>() : null);
	}

	private final DataType define(
			String parent,
			String name,
			InitValue initvalue) {
		return init(parent, name, (DataTypeContextInit)c->c.init = initvalue);
	}
	
	public final DataType define(InitValue initvalue) {
		return define(null, Concatenate.execute("type_", anonc++), initvalue);
	}
	
	public final DataType get(String name) {
		if (!types.containsKey(name))
			Fail.raise("type '%s' is invalid.", name);
		
		return types.get(name);
	}
	
	public final String getBoxedFor(String type) {
		return boxed.get(type);
	}
	
	private final DataType init(
			String parent,
			String name,
			DataTypeContextInit ctxinit) {
		if (sealed)
			Fail.raise("can't define types in a sealed context");
		
		if (types.containsKey(name))
			Fail.raise("type '%s' already exists.", name);
		
		var type = new DataType(this, parent, name, ctxinit);
		if (name != null)
			types.put(name, type);
		return type;
	}
	
	public final DataType object(
			DataType etype,
			String name,
			DataTypeContextInit ctxinit) {
		var type = init(null, name, ctxinit);
		type.parameter(etype);
		return type;
	}
	
	private final DataType primitive(String name, InitValue initvalue) {
		var type = define(null, name, initvalue);
		type.primitive();
		
		var bname = Concatenate.execute(name, "_value");
		var value_t = define(bname);
		value_t.add(type, "value");
		value_t.boxed();

		boxed.put(name, bname);
		
		return type;
	}
	
	public final void seal() {
		for (var key : types.keySet())
			types.get(key).seal();
		sealed = true;
	}
	
	public final Set<String> types() {
		return types.keySet();
	}
}
