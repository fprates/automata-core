package org.quanticsoftware.automata.core;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class DataType {
	private TypeContext typectx;
	private Map<String, ItemContext> items;
	private Set<DataType> parameters;
	private boolean sealed, autogenkey, primitive, boxed;
	private String key, name, aname;
	private DataTypeContext context;
	private DataTypeExtensionConfig extconfig;
	
	public DataType(
			TypeContext typectx,
			String parent,
			String name,
			DataTypeContextInit ctxinit) {
		this.typectx = typectx;
		this.name = name;
		parameters = new LinkedHashSet<>();
		items = new LinkedHashMap<>();
		
		ctxinit.init(context = new DataTypeContext());
		aname = (parent == null)? name : Concatenate.execute(parent, "_", name);
	}
	
	public final void add(String... names) {
		add(typectx.get("any"), names);
	}
	
	public final void add(DataType type, String... names) {
		add(type, false, names);
	}
	
	private final void add(DataType type, boolean reference, String... names) {
		if (sealed)
			Fail.raise("can't extend a sealed type for %s.", name);
		
		if (names == null)
			Fail.raise("undefined attribute name for %s.", name);
		
		for (var name : names) {
			var itemctx = new ItemContext(type, reference);
			items.put(name, itemctx);
			type.config(itemctx);
		}
		
		context.isstructure = true;
	}
	
	public final void adda(String... names) {
		add(typectx.get("amount"), names);
	}
	
	public final void addbl(String... names) {
		add(typectx.get("boolean"), names);
	}
	
	public final void addd(String... names) {
		add(typectx.get("double"), names);
	}
	
	public final void addi(String... names) {
		add(typectx.get("int"), names);
	}
	
	public final void addl(String... names) {
		add(typectx.get("long"), names);
	}
	
	public final DataType addo(String name) {
		var type = typectx.define(this, name);
		add(type, name);
		return type;
	}
	
	public final void addst(String... names) {
		add(typectx.get("string"), names);
	}
	
	public final String aname() {
		return aname;
	}
	
	public final void boxed() {
		boxed = true;
	}
	
	public final void config(ItemContext itemctx) {
		if (extconfig != null)
			extconfig.execute(itemctx.extension);
	}
	
	@Override
	public final boolean equals(Object object) {
		if (object == null)
			return false;
		
		if (this == object)
			return true;
		
		if (!(object instanceof DataType))
			return false;
		
		var test = (DataType)object;
		return name.equals(test.name());
	}
	
	public final DataTypeExtension extension(String name) {
		return items.get(name).extension;
	}
	
	public final DataType get(String name) {
		var item = items.get(name);
		if (item == null)
			Fail.raise("'%s' is not an element of '%s'.", name, this.name);
		return (item == null)? null : item.type;
	}
	
	public final DataTypeContext getContext() {
		return context;
	}
	
	public final boolean isAutoGenKey() {
		return autogenkey;
	}
	
	public final boolean isBoxed() {
		return boxed;
	}
	
	public final boolean isPrimitive() {
		return primitive;
	}
	
	public final boolean isReference(String name) {
		var item = items.get(name);
		if (item == null)
			Fail.raise("'%s' is not an element of '%s'.", name, this.name);
		
		return (item == null)? null : item.reference;
	}
	
	public final boolean isSealed() {
		return sealed;
	}
	
	public final boolean isStructure() {
		return context.isstructure;
	}
	
	public final Set<String> items() {
		return items.keySet();
	}
	
	public final String key() {
		return key;
	}
	
	public final void key(String key) {
		this.key = key;
	}
	
	public final String name() {
		return name;
	}
	
	public final void parameter(DataType type) {
		parameters.add(type);
	}
	
	public final void primitive() {
		primitive = true;
	}
	
	public final void ref(DataType type, String... names) {
		add(type, true, names);
	}
	
	public final void seal() {
		if (sealed)
			return;
		
		sealed = true;
		for (var item : items.keySet())
			items.get(item).type.seal();
	}
	
	public final void set(DataTypeExtensionConfig extconfig) {
		this.extconfig = extconfig;
	}
	
	public final void setAutoGenKey(boolean autogenkey) {
		this.autogenkey = autogenkey;
	}
	
	@Override
	public final String toString() {
		return Concatenate.execute(name, ": ", items.toString());
	}
}

class ItemContext {
	public DataType type;
	public boolean reference;
	public DataTypeExtension extension;
	
	public ItemContext(DataType type, boolean reference) {
		this.type = type;
		this.reference = reference;
		extension = new DataTypeExtension();
		extension.dbinput = (t,s)->s;
	}
	
	@Override
	public final String toString() {
		return type.toString();
	}
}
