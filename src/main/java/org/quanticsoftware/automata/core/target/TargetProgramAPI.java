package org.quanticsoftware.automata.core.target;

import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.facilities.Facility;
import org.quanticsoftware.automata.facilities.Function;
import org.quanticsoftware.automata.facilities.FunctionRule;

public class TargetProgramAPI {
	private Target target;
	private int anonc;
	
	public TargetProgramAPI(Target target) {
		this.target = target;
	}
	
	public final void allinput(
			DataType output,
			Facility facility,
			String name,
			FunctionRule rule) {
		facility.function(name).allinput(output, rule);
		target.steps(compose(facility, name));
	}
	
	public final void allinput(
			DataType output,
			Facility facility,
			FunctionRule rule) {
		allinput(output, facility, getAnonId(), rule);
	}
	
	public final void bypass(
			DataType output,
			Facility facility,
			String name,
			FunctionRule rule) {
		facility.function(name).bypass(output, rule);
		target.steps(compose(facility, name));
	}
	
	public final void bypass(
			DataType output,
			Facility facility,
			FunctionRule rule) {
		bypass(output, facility, getAnonId(), rule);
	}
	
	public final void call(String... names) {
		target.steps(names);
	}
	
	public final void collect(DataType type, Facility facility, String field) {
		var name = getAnonId();
		facility.function(name).collect(type, field);
		target.steps(compose(facility, name));
	}
	
	private final String compose(Facility facility, String function) {
		return Concatenate.execute(facility.name(), ".", function);
	}
	
	public final void fail(Target target) {
		this.target.fail(target);
	}
	
	public final Function function(
			DataType output,
			Facility facility,
			String name,
			FunctionRule rule) {
		var function = facility.function(name);
		function.rule(output, rule);
		target.steps(compose(facility, name));
		return function;
	}
	
	public final Function function(
			DataType output,
			Facility facility,
			FunctionRule rule) {
		return function(output, facility, getAnonId(), rule);
	}
	
	private final String getAnonId() {
		return Concatenate.execute(target.name(), "_anonymous_", anonc++);
	}
	
	public final void setExceptionAsMessage(boolean exasmsg) {
		target.setExceptionAsMessage(exasmsg);
	}
}
