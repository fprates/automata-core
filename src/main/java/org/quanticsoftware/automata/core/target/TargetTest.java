package org.quanticsoftware.automata.core.target;

import java.util.LinkedList;
import java.util.List;

import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.DataType;

public class TargetTest {
	public final DataContext datactx;
	private String oname, fail;
	private Target target;
	private List<DataObject> objects;
	private OutputTest otest;
	
	public TargetTest(Target target) {
		datactx = new DataContext();
		objects = new LinkedList<>();

		this.target = target;
		for (var key : target.input())
			datactx.instance(target.input(key), key);
		
	}
	
	public final void fail(String message) {
		fail = message;
	}
	
	public final List<DataObject> getDBObjects() {
		return objects;
	}
	
	public final String getFailMessage() {
		return fail;
	}
	
	public final OutputTest getOutputTest() {
		return otest;
	}
	
	public final DataObject insert(DataType type) {
		var object = datactx.get(datactx.instance(type));
		objects.add(object);
		return object;
	}
	
	public final DataObject output() {
		if (oname == null)
			oname = datactx.instance(target.getOutputType());
		return datactx.get(oname);
	}
	
	public final void output(OutputTest otest) {
		this.otest = otest;
	}
	
	public final void seal() {
		for (var key : target.input())
			datactx.get(key).seal();
	}
}
