package org.quanticsoftware.automata.core.target;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.core.Fail;

public class Target {
	private String name;
	private List<TargetStep> steps;
	private boolean exasmsg;
	private Map<String, TargetTest> tests;
	private DataType otype;
	private Map<String, DataType> input;
	public final Map<String, Target> fail;
	
	public Target(String name) {
		fail = new HashMap<>();
		steps = new LinkedList<>();
		tests = new HashMap<>();
		input = new HashMap<>();
		this.name = name;
	}
	
	public final void add(Target... targets) {
		for (var target : targets) {
			if (target == null)
				Fail.raise("invalid target in target.add()");
			this.steps.add(new TargetStep(null, target));
		}
	}
	
	public final void fail(Target target) {
		fail.put("default", target);
	}
	
	public final void fail(String name, Target target) {
		fail.put(name, target);
	}
	
	public final DataType getOutputType() {
		return otype;
	}
	
	public final TargetTest getTest(String name) {
		return tests.get(name);
	}
	
	public final Set<String> input() {
		return input.keySet();
	}
	
	public final DataType input(String name) {
		return input.get(name);
	}
	
	public final void input(String name, DataType type) {
		input.put(name, type);
	}
	
	public final boolean isExceptionAsMessage() {
		return exasmsg;
	}
	
	public final String name() {
		return name;
	}
	
	public final void output(DataType type) {
		otype = type;
	}
	
	public final void program(TargetProgram program) {
		program.execute(new TargetProgramAPI(this));
	}
	
	public final List<TargetStep> steps() {
		return steps;
	}
	
	public final void setExceptionAsMessage(boolean exasmsg) {
		this.exasmsg = exasmsg;
	}
	
	public final void steps(String... steps) {
		for (var step : steps)
			this.steps.add(new TargetStep(step, null));
	}
	
	public final TargetTest test(String name) {
		if (tests.containsKey(name))
			Fail.raise("test '%s' can't be overriden.", name);
		
		var test = new TargetTest(this);
		tests.put(name, test);
		return test;
	}
	
	public final Set<String> tests() {
		return tests.keySet();
	}
}
