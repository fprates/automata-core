package org.quanticsoftware.automata.core.target;

public class TargetStep {
	public String alias;
	public Target target;
	
	public TargetStep(String alias, Target target) {
		this.alias = alias;
		this.target = target;
	}
}
