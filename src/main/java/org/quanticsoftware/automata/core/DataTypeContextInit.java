package org.quanticsoftware.automata.core;

public interface DataTypeContextInit {
	
	public abstract void init(DataTypeContext context);
	
}
