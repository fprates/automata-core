package org.quanticsoftware.automata.core;

public interface InitValue {
	
	public abstract Object get(DataTypeContext context);
	
}
