package org.quanticsoftware.automata.core;

import java.util.Map;

public class Conversion {
	
	@SuppressWarnings("unchecked")
	public static final void execute(
			DataContext datactx,
			DataType otype,
			Map<String, Object> data) {
		
		for (var key : otype.items()) {
			if (!data.containsKey(key))
				continue;
			
			var itype = otype.get(key);
			var ext = otype.extension(key);
			
			if (ext.oconversion != null) {
				data.put(key, ext.oconversion.execute(null, data.get(key)));
				continue;
			}
			
			if (otype.isReference(key)) {
				var idata = (Map<String, Object>)data.get(key);
				var oname = datactx.instance(itype, idata);
				data.put(key, datactx.get(oname));
				execute(datactx, itype, idata);
				continue;
			}
			
			if (itype.isStructure())
				execute(datactx, itype, (Map<String, Object>)data.get(key));
		}
	}
	
}
