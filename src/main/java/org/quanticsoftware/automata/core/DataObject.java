package org.quanticsoftware.automata.core;

import java.util.HashMap;
import java.util.Map;

public class DataObject {
	private DataType type;
	private Map<String, Object> data;
	private boolean sealed;
	
	public DataObject(DataType type, Map<String, Object> data) {
		this.type = type;
		this.data = data;
	}
	
	@SuppressWarnings("unchecked")
	public final void build(Map<String, Object> data, DataType type) {
		
		for (var titem : type.items()) {
			if (type.isReference(titem)) {
				data.put(titem, null);
				continue;
			}
			
			var ctype = type.get(titem);
			var context = ctype.getContext();
			var cdata = context.init.get(context);
			data.put(titem, cdata);
			
			if (ctype.isStructure())
				build((Map<String, Object>)cdata, ctype);
		}
	}
	
	public final void copy(DataObject object) {
		var otype = object.type;
		
		for (var key : type.items())
			if (otype.get(key) != null)
				set(key, object.get(key));
	}
	
	public final Map<String, Object> data() {
		return data;
	}
	
	public final void data(Map<String, Object> data) {
		this.data = data;
	}
	
	@SuppressWarnings("unchecked")
	public final Map<String, Object> data(String name) {
		if (sealed)
			Fail.raise("can't update a sealed data object.");
		return (Map<String, Object>)get(name);
	}
	
	public final Object get(String name) {
		var names = name.split("\\.");
		return getValues(names).get(names[names.length - 1]);
	}
	
	public final boolean getbl(String name) {
		var value = get(name);
		return (value == null)? false : (boolean)value;
	}
	
	public final double getd(String name) {
		var value = get(name);
		return (value == null)? 0 : (double)value;
	}
	
	public final int geti(String name) {
		var value = get(name);
		return (value == null)? 0 : (int)value;
	}
	
	public final long getl(String name) {
		var value = get(name);
		return (value == null)? 0l : (long)value;
	}
	
	public final String getst(String name) {
		return (String)get(name);
	}
	
	@SuppressWarnings("unchecked")
	private final Map<String, Object> getValues(String[] names) {
		var values = data;
		var itype = type;
		
		for (int i = 0; i < (names.length - 1); i++) {
			if (itype.get(names[i]) == null)
				Fail.raise("'%s' is an invalid element in '%s'.",
						names[i],
						itype.name());
			
			itype = itype.get(names[i]);
			values = (Map<String, Object>)values.get(names[i]);
		}
		
		if (itype.isPrimitive())
			return values;
		
		var i = names.length - 1;
		if (itype.get(names[i]) == null)
			Fail.raise(
					"'%s' is an invalid element in '%s'.",
					names[i],
					itype.name());
		
		return values;
	}
	
	public final void init() {
		build(data = new HashMap<>(), type);
	}
	
	public final void seal() {
		sealed = true;
	}
	
	public final void set(String name, Object value) {
		if (sealed)
			Fail.raise("can't update a sealed data object.");
		
		var names = name.split("\\.");
		getValues(names).put(names[names.length - 1], value);
	}
	
	public final void set(double value, String unit) {
		set("value", value);
		set("unit", unit);
	}
	
	@Override
	public final String toString() {
		return (data == null)? super.toString() : data.toString();
	}
	
	public final DataType type() {
		return type;
	}
}
