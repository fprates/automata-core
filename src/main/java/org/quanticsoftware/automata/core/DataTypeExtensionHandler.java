package org.quanticsoftware.automata.core;

public interface DataTypeExtensionHandler {
	
	public abstract Object execute(Object target, Object source);
	
}
