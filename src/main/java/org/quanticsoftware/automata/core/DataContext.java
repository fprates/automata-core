package org.quanticsoftware.automata.core;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DataContext {
	private Map<String, DataObjectContext> objects;
	private Map<String, Long> typecounters;
	private Map<String, Set<String>> references;
	private Set<String> instances;
	
	public DataContext() {
		objects = new HashMap<>();
		typecounters = new HashMap<>();
		references = new HashMap<>();
		instances = new HashSet<>();
	}
	
	public final void add(String name, DataObject object) {
		var type = object.type();
		
		reference(type, name);
		index(type, object, null, name);
		instances.add(name);
	}
	
	public final void clear() {
		for (var key : references.keySet())
			references.get(key).clear();
		
		references.clear();
		objects.clear();
		instances.clear();
	}

	@SuppressWarnings("unchecked")
	public final DataObject get(String name) {
		Map<String, Object> data = null;
		
		var dataobjctx = objects.get(name);
		if (dataobjctx == null)
			return null;
		
		for (var token : dataobjctx.tokens) {
			if (data == null) {
				data = objects.get(token).object.data();
				continue;
			}
			
			data = (Map<String, Object>)data.get(token);
			if (data == null)
				return null;
		}
		dataobjctx.object.data(data);
		return dataobjctx.object;
	}
	
	private final long getCounter(String name) {
		var counter = typecounters.containsKey(name)?
				typecounters.get(name) : 0;
		typecounters.put(name, counter + 1);
		return counter;
	}
	
	private final String getObjectName(DataType type) {
		var tname = type.name();
		var counter = getCounter(tname);
		return Concatenate.execute(tname, counter);
	}
	
	private final void index(
			DataType type,
			DataObject object,
			List<String> ptokens,
			String name) {
		
		var dataobjctx = new DataObjectContext();
		
		if (object == null) {
			dataobjctx.object = new DataObject(type, null);
			dataobjctx.tokens.addAll(ptokens);
			dataobjctx.tokens.add(type.name());
		} else {
			dataobjctx.object = object;
			dataobjctx.tokens.add(name);
		}
		
		objects.put(name, dataobjctx);
		
		for (var item : type.items())
			index(
					type.get(item),
					null,
					dataobjctx.tokens,
					Concatenate.execute(name, item));
	}
	
	private final DataObject instance(
			DataType type,
			String name,
			Map<String, Object> data,
			boolean init) {
		
		if (!type.isSealed())
			Fail.raise("base type '%s' not sealed.", type.name());

		var object = new DataObject(type, data);
		
		if (init)
			object.init();
		
		reference(type, name);
		index(type, object, null, name);
		instances.add(name);
		
		return object;
	}
	
	public final DataObject instance(DataType type, String name) {
		return instance(type, name, null, true);
	}
	
	public final DataObject instance(
			DataType type,
			String name,
			Map<String, Object> data) {
		return instance(type, name, data, false);
	}
	
	public final String instance(TypeContext typectx, String tname, boolean init)
	{
		var type = typectx.get(tname);
		var name = getObjectName(type);
		instance(type, name, null, init);
		return name;
	}
	
	public final String instance(DataType type) {
		var name = getObjectName(type);
		instance(type, name, null, true);
		return name;
	}
	
	public final String instance(DataType type, Map<String, Object> data) {
		var name = getObjectName(type);
		instance(type, name, data, false);
		return name;
	}
	
	public final DataObject instance(TypeContext typectx, String tname) {
		var name = instance(typectx, tname, true);
		return get(name);
	}
	
	public final DataObject instance(
			TypeContext typectx,
			String tname,
			String name) {
		return instance(typectx.get(tname), name);
	}
	
	public final Set<String> instances() {
		return instances;
	}
	
	private final void reference(DataType type, String name) {
		var tname = type.name();
		var objects = references.computeIfAbsent(tname, k->new HashSet<>());
		objects.add(name);
		
		for (var item : type.items())
			if (!type.isReference(item))
				reference(type.get(item), Concatenate.execute(name, item));
	}
	
	public final Set<String> references(String type) {
		return references.get(type);
	}
	
	public final void remove(String name) {
		var object = objects.get(name).object;
		var type = object.type();
		
		for (var item : type.items())
			remove(Concatenate.execute(name, item));
		
		references.get(type.name()).remove(name);
		objects.remove(name);
	}
}

class DataObjectContext {
	public DataObject object;
	public List<String> tokens;
	
	public DataObjectContext() {
		tokens = new LinkedList<>();
	}
}
